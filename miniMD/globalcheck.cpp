/* ----------------------------------------------------------------------
   miniMD is a simple, parallel molecular dynamics (MD) code.   miniMD is
   an MD microapplication in the Mantevo project at Sandia National
   Laboratories ( http://software.sandia.gov/mantevo/ ). The primary
   authors of miniMD are Steve Plimpton and Paul Crozier
   (pscrozi@sandia.gov).

   Copyright (2008) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This library is free software; you
   can redistribute it and/or modify it under the terms of the GNU Lesser
   General Public License as published by the Free Software Foundation;
   either version 3 of the License, or (at your option) any later
   version.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this software; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA.  See also: http://www.gnu.org/licenses/lgpl.txt .

   For questions, contact Paul S. Crozier (pscrozi@sandia.gov).

   Please read the accompanying README and LICENSE files.
---------------------------------------------------------------------- */

#include "stdio.h"
#include "atom.h"
#include <gds.h>
#include <cassert>

// Global error check
// check if current position of the mass center equals prediction
// if current position does not equal prediction, return 1, else return 0
int globalcheck(Atom &atom) {

    int i, j, nlocal;
    double **x, **v;
    int rank, size;
    double CurrentCenter[3] = {0, 0, 0};
    double CurrentVelocity[3] = {0, 0, 0};
    MPI_Status status;
    double TempX[3] = {0, 0, 0};
    double TempV[3] = {0, 0, 0};
    int Total = 0, temp = 0;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    x = atom.x;
    v = atom.v;
    nlocal = atom.nlocal;
    for(i = 0; i < nlocal; i++) {
        for(j = 0; j < 3; j++) {
            CurrentCenter[j] += x[i][j];
            CurrentVelocity[j] += v[i][j];
        }
    }

    if(rank != 0 ) {
        MPI_Send(&CurrentCenter, 3, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
        MPI_Send(&CurrentVelocity, 3, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
        MPI_Send(&nlocal, 1, MPI_INT, 0, 2, MPI_COMM_WORLD);
    }
    else if(rank == 0) {
        for(i = 0; i < 3; i++) {
            TempX[i] = 0;
            TempV[i] = 0;
        }
        Total = atom.nlocal;
        temp = 0;
        for(i = 1; i < size; i++) {
            MPI_Recv(&TempX, 3, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, &status);
            MPI_Recv(&TempV, 3, MPI_DOUBLE, i, 1, MPI_COMM_WORLD, &status);
            MPI_Recv(&temp, 1, MPI_INT, i, 2, MPI_COMM_WORLD, &status);
            Total += temp;
            for(j = 0; j < 3; j++) {
                CurrentCenter[j] += TempX[j];
                CurrentVelocity[j] += TempV[j];
            }
        }
        printf("Total number of atoms: %d\n", Total);
        // compare CurrentCenter with PredictionCenter
        printf("CurrentCenter: %f %f %f\n", CurrentCenter[0], CurrentCenter[1], CurrentCenter[2]);
        printf("PredictionCenter: %f %f %f\n", atom.PredictionCenter[0], atom.PredictionCenter[1], atom.PredictionCenter[2]);
        for(i = 0; i < 3; i++) {
            atom.PredictionCenter[i] = CurrentCenter[i];
        }
    }

    return 0;
}

void calc_avg_vector(double **v, int n, double avgv_r[3])
{
  double sumv[3] = { 0.0, 0.0, 0.0 };
  int nsum = n;

  assert(n > 0);
  assert(v);

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < 3; j++)
      sumv[j] += v[i][j];
  }

  MPI_Allreduce(MPI_IN_PLACE, sumv, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &nsum, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  for (int j = 0; j < 3; j++)
    avgv_r[j] = sumv[j] / nsum;
}

void predict_mass_center(const double initx[3], const double initv[3], double dt, double retx[3])
{
  for (int i = 0; i < 3; i++)
    retx[i] = initx[i] + initv[i] * dt;
}
