/* ----------------------------------------------------------------------
   miniMD is a simple, parallel molecular dynamics (MD) code.   miniMD is
   an MD microapplication in the Mantevo project at Sandia National
   Laboratories ( http://software.sandia.gov/mantevo/ ). The primary
   authors of miniMD are Steve Plimpton and Paul Crozier
   (pscrozi@sandia.gov).

   Copyright (2008) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This library is free software; you
   can redistribute it and/or modify it under the terms of the GNU Lesser
   General Public License as published by the Free Software Foundation;
   either version 3 of the License, or (at your option) any later
   version.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this software; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA.  See also: http://www.gnu.org/licenses/lgpl.txt .

   For questions, contact Paul S. Crozier (pscrozi@sandia.gov).

   Please read the accompanying README and LICENSE files.
---------------------------------------------------------------------- */

#include "stdio.h"
#include "integrate.h"
#include <gds.h>
#include <cassert>
#include <stdexcept>

int errordetect(const Atom &atom, double tol);
int globalcheck(Atom &atom);
int injectfault(Atom &atom);
void inject_fault_det(Atom &atom);
void calc_avg_vector(double **v, int n, double avgv_r[3]);
void predict_mass_center(const double initx[3], const double initv[3], double dt, double retx[3]);

struct atom_metadata {
  int nlocal, nghost;
  struct Box box;
  int n; // iteration
};

static void save_md(const Atom& atom, struct atom_metadata *md, int n)
{
  md->nlocal = atom.nlocal;
  md->nghost = atom.nghost;
  md->box    = atom.box;
  md->n      = n;
}

static void restore_md(const struct atom_metadata &md, Atom *atom, int *n)
{
  atom->nlocal = md.nlocal;
  atom->nghost = md.nghost;
  atom->box    = md.box;
  *n           = md.n;
}

int Integrate::recover(Atom &atom, Neighbor &neighbor, int nmax,
                        GDS_gds_t gds_x, GDS_gds_t gds_v, GDS_gds_t gds_f, GDS_gds_t gds_vold, GDS_gds_t gds_md)
{
  GDS_gds_t x2, v2, f2, vold2, md2;
  struct atom_metadata atom_md;
  int rank, n;
  size_t size;
  GDS_comm_rank(GDS_COMM_WORLD, &rank);
  GDS_comm_size(GDS_COMM_WORLD, &size);

  GDS_size_t lo[2] = {nmax * rank, 0}, hi[2] = {nmax * (rank+1) -1, 3-1};
  GDS_size_t mlo[1] = { sizeof(struct atom_metadata) * rank };
  GDS_size_t mhi[1] = { sizeof(struct atom_metadata) * (rank+1) -1 };
  GDS_size_t ld[1] = {3}, ld_md[] = {};

  GDS_descriptor_clone(gds_x, &x2);
  GDS_descriptor_clone(gds_v, &v2);
  GDS_descriptor_clone(gds_f, &f2);
  GDS_descriptor_clone(gds_vold, &vold2);
  GDS_descriptor_clone(gds_md, &md2);
  assert(x2 && v2 && f2 && md2);

  for (;;) {
    int flag;
    GDS_size_t ver;
    GDS_get_version_number(x2, &ver);
    printf("Process #%d: Investigating version #%zu\n", rank, ver);

    GDS_get((void *)atom.x[0], ld, lo, hi, x2);
    GDS_get((void *)atom.v[0], ld, lo, hi, v2);
    GDS_get((void *)atom.f[0], ld, lo, hi, f2);
    GDS_get((void *)atom.vold[0], ld, lo, hi, vold2);
    GDS_get(&atom_md, ld_md, mlo, mhi, md2);
    restore_md(atom_md, &atom, &n);

    flag = neighbor.atoms_error_check(atom) || errordetect(atom, neighbor.cutneigh*2);
    MPI_Allreduce(MPI_IN_PLACE, &flag, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
    if (!flag) {
      // good version
      printf("Process #%d: Going back to version #%zu\n", rank, ver);
      break;
    }

    if (GDS_move_to_prev(x2) != GDS_STATUS_OK) {
      printf("Process #%d: No more old versions... give up!\n", rank);
      break;
    }
    GDS_move_to_prev(v2);
    GDS_move_to_prev(f2);
    GDS_move_to_prev(vold2);
    GDS_move_to_prev(md2);
  }

  GDS_free(&x2);
  GDS_free(&v2);
  GDS_free(&f2);
  GDS_free(&vold2);
  GDS_free(&md2);

  return n;
}

Integrate::Integrate() {}
Integrate::~Integrate() {}

void Integrate::setup()
{
  dtforce = 48.0*dt;
}

void Integrate::run(Atom &atom, Force &force, Neighbor &neighbor,
		    Comm &comm, Thermo &thermo, Timer &timer)
{
  int i,n,nlocal;
  int rank;
  size_t size;
  double **x,**v,**f,**vold;
  GDS_gds_t  gds_x, gds_v, gds_f, gds_vold, gds_md;
  struct atom_metadata atom_md;
  int nmax = atom.nmax;
  // double initxc[3], initvc[3], curxc[3], predxc[3];
  bool injected = false;
  const int CP_INTERVAL = 5;

  /*
  calc_avg_vector(atom.x, atom.nlocal, initxc);
  calc_avg_vector(atom.v, atom.nlocal, initvc);
  */

  GDS_comm_rank(GDS_COMM_WORLD, &rank);
  GDS_comm_size(GDS_COMM_WORLD, &size);

  MPI_Allreduce(MPI_IN_PLACE, &nmax, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);

  GDS_size_t xvf_size [2] = {nmax * size, 3};
  GDS_size_t min_chunk[2] = {0, 0};
  GDS_size_t mdsize[1] = { sizeof(struct atom_metadata) * size };

  GDS_alloc(2, xvf_size, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, NULL, &gds_x);
  GDS_alloc(2, xvf_size, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, NULL, &gds_v);
  GDS_alloc(2, xvf_size, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, NULL, &gds_f);
  GDS_alloc(2, xvf_size, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, NULL, &gds_vold);
  GDS_alloc(1, mdsize, min_chunk, GDS_DATA_BYTE, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, NULL, &gds_md);
  assert(gds_x && gds_v && gds_f && gds_vold && gds_md);

  for (n = 0; n < ntimes; n++) {
    GDS_size_t lo[2] = {nmax * rank, 0}, hi[2] = {nmax * (rank+1) -1, 3-1};
    GDS_size_t mlo[1] = { sizeof(struct atom_metadata) * rank };
    GDS_size_t mhi[1] = { sizeof(struct atom_metadata) * (rank+1) -1 };
    GDS_size_t ld[1] = {3}, ld_md[] = {};
    int err_found = 0;

    x = atom.x;
    v = atom.v;
    nlocal = atom.nlocal;

    for (i = 0; i < nlocal; i++) {
      x[i][0] += dt*v[i][0];
      x[i][1] += dt*v[i][1];
      x[i][2] += dt*v[i][2];
    }

    if (n == 18 && rank == 0 && !injected) {
      // Inject Fault: One bit flip
      printf("\x1b[1m\x1b[4m");
      printf("n=%d: Injecting error to process #%d\n", n, rank);
      printf("\x1b[0m");
      injected = true;
	  inject_fault_det(atom);
    }

    timer.stamp();

    if ((n+1) % neighbor.every) {
      comm.communicate(atom);
      timer.stamp(TIME_COMM);
    } else {
      comm.exchange(atom);
      comm.borders(atom);
      timer.stamp(TIME_COMM);
      try {
        neighbor.build(atom);
      } catch (std::exception& e) {
        printf("\x1b[1m\x1b[4m");
	    printf("n=%d: Process #%d Exception: %s\n", n, rank, e.what());
        printf("\x1b[0m");
	    err_found = 1;
	    goto reduce;
      }
      timer.stamp(TIME_NEIGH);
    }
    if (atom.nmax > nmax) {
      printf("Array grew -- unsupported!\n");
      exit(1);
    }

    /* TODO: dirty magic number... */
    err_found = errordetect(atom, neighbor.cutneigh*2);
    if (err_found) {
      printf("\x1b[1m\x1b[4m");
      printf("Process #%d detected an error. n=%d\n", rank, n);
      printf("\x1b[0m");
    }

    force.compute(atom,neighbor,comm.me);
    timer.stamp(TIME_FORCE);

    comm.reverse_communicate(atom);
    timer.stamp(TIME_COMM);

    vold = atom.vold;
    v = atom.v;
    f = atom.f;
    nlocal = atom.nlocal;

    for (i = 0; i < nlocal; i++) {
      vold[i][0] = v[i][0];
      vold[i][1] = v[i][1];
      vold[i][2] = v[i][2];
      v[i][0] += dtforce*f[i][0];
      v[i][1] += dtforce*f[i][1];
      v[i][2] += dtforce*f[i][2];
    }

    if (thermo.nstat) thermo.compute(n+1,atom,neighbor,force);

  reduce:
	MPI_Allreduce(MPI_IN_PLACE, &err_found, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
    if (err_found) {
      n = recover(atom, neighbor, nmax, gds_x, gds_v, gds_f, gds_vold, gds_md);
      continue;
    }

    /*
    calc_avg_vector(atom.x, atom.nlocal, curxc);
    predict_mass_center(initxc, initvc, dt*(n+1), predxc);
    printf("n=%d\n", n);
    for (int j = 0; j < 3; j++)
      printf("  center[%d]=%f (predicted=%f)\n", j, curxc[j], predxc[j]);
    printf("  dt=%f dtforce=%f\n", dt, dtforce);
    */

    if (n % CP_INTERVAL == 0) {
      /* Take a checkpoint */
      if (rank == 0) {
        GDS_size_t ver;
        GDS_get_version_number(gds_x, &ver);
        printf("n=%d: Taking checkpoint of version #%zu\n", n, ver);
      }
      GDS_put((void *)atom.x[0], ld, lo, hi, gds_x);
      GDS_put((void *)atom.v[0], ld, lo, hi, gds_v);
      GDS_put((void *)atom.f[0], ld, lo, hi, gds_f);
      GDS_put((void *)atom.vold[0], ld, lo, hi, gds_vold);
      save_md(atom, &atom_md, n);
      GDS_put(&atom_md, ld_md, mlo, mhi, gds_md);

      GDS_fence(gds_x);
      GDS_fence(gds_v);
      GDS_fence(gds_f);
      GDS_fence(gds_vold);
      GDS_fence(gds_md);

      // versioning
      GDS_version_inc(gds_x, 1, NULL, 0);
      GDS_version_inc(gds_v, 1, NULL, 0);
      GDS_version_inc(gds_f, 1, NULL, 0);
      GDS_version_inc(gds_vold, 1, NULL, 0);
      GDS_version_inc(gds_md, 1, NULL, 0);
    }
  }

  GDS_free(&gds_x);
  GDS_free(&gds_v);
  GDS_free(&gds_f);
  GDS_free(&gds_vold);
  GDS_free(&gds_md);
}
