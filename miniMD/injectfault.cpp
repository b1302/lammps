/* ----------------------------------------------------------------------
   miniMD is a simple, parallel molecular dynamics (MD) code.   miniMD is
   an MD microapplication in the Mantevo project at Sandia National
   Laboratories ( http://software.sandia.gov/mantevo/ ). The primary
   authors of miniMD are Steve Plimpton and Paul Crozier
   (pscrozi@sandia.gov).

   Copyright (2008) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This library is free software; you
   can redistribute it and/or modify it under the terms of the GNU Lesser
   General Public License as published by the Free Software Foundation;
   either version 3 of the License, or (at your option) any later
   version.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this software; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA.  See also: http://www.gnu.org/licenses/lgpl.txt .

   For questions, contact Paul S. Crozier (pscrozi@sandia.gov).

   Please read the accompanying README and LICENSE files.
---------------------------------------------------------------------- */

#include "stdio.h"
#include "atom.h"
#include <gds.h>
#include "stdlib.h"
#include "time.h"

// Inject fault
int injectfault(Atom &atom) {

    int i, idim, nlocal;
    int random;

    nlocal = atom.nlocal;

    srand((unsigned)time(NULL));
    random = rand()%nlocal;
    idim = rand()%3;
    i = rand()%10;

    atom.x[random][idim] = atom.x[random][idim] + i;

    return 0;
}

/* Inject fault in a deterministic manner */
void inject_fault_det(Atom &atom)
{
  int i = 127;

  if (atom.nlocal <= i)
    i = atom.nlocal-1;

  atom.x[i][0] = -atom.x[i][0];
}
