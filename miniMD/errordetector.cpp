/* ----------------------------------------------------------------------
   miniMD is a simple, parallel molecular dynamics (MD) code.   miniMD is
   an MD microapplication in the Mantevo project at Sandia National
   Laboratories ( http://software.sandia.gov/mantevo/ ). The primary
   authors of miniMD are Steve Plimpton and Paul Crozier
   (pscrozi@sandia.gov).

   Copyright (2008) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This library is free software; you
   can redistribute it and/or modify it under the terms of the GNU Lesser
   General Public License as published by the Free Software Foundation;
   either version 3 of the License, or (at your option) any later
   version.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this software; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA.  See also: http://www.gnu.org/licenses/lgpl.txt .

   For questions, contact Paul S. Crozier (pscrozi@sandia.gov).

   Please read the accompanying README and LICENSE files.
---------------------------------------------------------------------- */

#include "stdio.h"
#include "atom.h"
#include <gds.h>
#include <math.h>

// detecting errors of local buffer
// check if the atoms are in the right box, whether the atoms belong to my box
// if find some atoms don't belong to my box, return 1, else return 0
int errordetect(const Atom &atom, double tol) {

    int i, idim, nlocal;
    double lo, hi;
    double **x;

    for(idim = 0; idim < 3; idim++) {

        if(idim == 0) {
            lo = atom.box.xlo;
            hi = atom.box.xhi;
        }
        else if(idim == 1) {
            lo = atom.box.ylo;
            hi = atom.box.yhi;
        }
        else if(idim == 2) {
            lo = atom.box.zlo;
            hi = atom.box.zhi;
        }

        x = atom.x;
        nlocal = atom.nlocal;
        i = 0;

        while (i < nlocal) {
            if (x[i][idim] < lo-tol || x[i][idim] > hi+tol) {
              /*printf("x[%d][%d]=%f lo-tol=%f hi+tol=%f tol=%f\n", i, idim, x[i][idim],
                lo-tol, hi+tol, tol);*/
                return 1;
            }
            i++;
        }

    }

    return 0;
}
